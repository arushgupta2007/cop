# COmpetitive Programming - COP

A small program to help you in your competitive programming journey!

## Getting Started

Currently, the only way to get this, is to clone the repo and compile the
project.

**Note**: This program is tested only in GNU/Linux. It may not work in Windows or MacOS.

1. Make sure you have [Rust](https://www.rust-lang.org/), [Git](https://git-scm.com/), [Sqlite](https://www.sqlite.org/index.html)
   installed.
2. Run 
    ```sh
    cargo install --git https://gitlab.com/arushgupta2007/cop
    ```
3. Make a config file. Open `<config dir>/cop.toml` (`~/.config/cop` on GNU / Linux) 

    ```toml 
    contest_dir = '<Insert directory where you want the contests to be located>'
    problem_dir = '<Insert directory where you want the problems to be located>'
    template_path = '<Insert path to your cpp template file>'
    active_problem_id = 0 # Do not change this
    active_contest_id = 0 # or this 
    ```
    
4. Enjoy!

## Usage

### Contest

#### Create

Creates a contest in `contest_dir`. The contest contains directories for each problem. In each of
the problem directory, you will find `main.cpp` (your template), `problem.in` (the sample test
cases for the problem here), `problem.out` (expected output of sample test cases here),
`program.out` (output of your program on sample test cases here).

Inputs:

1. `-d` or `--date`: Date and time, when the contest starts. Enter in `%H:%M %d/%m/%Y` format
2. `-n` or `--no-problem`: Number of problems in the contest
3. `-p` or `--platform`: Platform on which the contest is being hosted on. Currently only supports
   `"CC"` (CodeChef), `"CF"` (Codeforces).
4. `-a` or `--activate`: A flag to activate the contest (Optional)
5. Name of contest

Example:

```sh
cop contest create "Educational Round 124" -d "20:05 10/03/2022" -n 7 -p CF -a
``` 
(Create a contest with name `"Educational Round 124"`, which starts at `20:05 10/03/2022`, with 7
problems, hosted on Codeforces, and activate it)

#### List

Lists all contests in the Database. For each contest, it will show the id, name, platform, no.
problems, date, no. of solved problems. Filter options include regex on name of contest, filter
contest before and after date.

Inputs:

1. `-r` or `--regex`: Regex to be matched on name of contest (Optional)
2. `-e` or `--end-date`: Do not show contests after this date. Enter in `%H:%M %d/%m/%Y` format
   (Optional)
3. `-s` or `--start-sate`: Do not show contests before this date. Enter in `%H:%M %d/%m/%Y` format
   (Optional)

Example:

```sh
cop contest list -r "Starters" -s "20:00 14/01/2022" -e "20:00 13/03/2022" 
```
(List all contests with `"Starters"` in the name, which have occurred after `20:00 14/01/2022`, and
before `20:00 13/03/2022`)

#### Activate

Activate a contest, and optionally a problem in the contest. The benifit of activating a contest,
is that, for other commands, it is not necessary to enter the contest ID.

Inputs:

1. `-p` or `--problem-id`: The problem number of the contest you want to activate.
2. ID of the contest

Example: 
```sh 
cop contest activate -p 4 3
```
(Activate Problem 4 of contest with ID 3)

#### Set Solved

After the contest ends, you can enter how many problems you were able to solve.

Inputs:

1. `-i` or `--id`: ID of contest (Optional, default is active contest)
2. How many problems you were able to solve

Example:

```sh
cop contest set-solved -i 3 5 
```
(Solved 5 problems in contest with ID 3)

#### Watch

Start watching for file changes given contest. Once a file changes, recompile the appropriate file,
and reevaluate on the given sample test cases.

Inputs:

1. `-i` or `--id`: ID of contest (Optional, default is activate contest)

Example: 
```sh
cop contest watch -i 3 
```
(Start Watching Contest with ID 3)


#### Delete

Delete given contest

Inputs:

1. ID of contest to delete

Example: 
``` cop
contest delete 3 
```
(Delete Contest with ID 3)


### Problem

#### Create

Creates a problem in `problem_dir`. In the problem directory, you will find `main.cpp` (your
template), `problem.in` (the sample test cases for the problem here), `problem.out` (expected
output of sample test cases here), `program.out` (output of your program on sample test cases
here).

Inputs:

1. `-p` or `--platform`: Platform on which the problem exists
2. `-a` or `--activate`: A flag to activate the contest (Optional)

Example: 
```sh
cop problem create -a -p CF "Chat Ban" 
```
(Create a problem with name `Chat Ban`, hosted on Codeforces, and activate it).

#### List

Lists all problems in the Database. For each contest, it will show the id, name, platform, solved
status, editorial status. Filter options include regex on name of contest, solved problems,
unsolved problems, problems with editorial, problems without editorial.

Inputs:

1. `-r` or `--regex`: Regex to be matched on name of problem (Optional)
2. `-e` or `--editorial`: Flag to only show problems with editorials (Optional)
3. `-n` or `--no-editorial`: Flag to only show problems without editorials (Optional)
4. `-s` or `--solved`: Flag to only show solved problems
5. `-u` or `--unsolved`: Flag to only show unsolved problems

Example: 
```sh
cop problem list -r "Tiling" -s -e 
```
(Show solved problems with the word `Tiling` in the problem name, and have an editorial)

#### Activate

Activate a problem. The benifit of activating a problem, is that, for other commands, it is not
necessary to enter the problem ID.

Inputs:

1. ID of the contest

Example: 
```sh
cop problem activate 3 
```
(Activate the problem with ID 3)

#### Toggle Solved

Toggle the solved state of the given problem.

Input:
1. `-i` or `--id`: ID of the problem (Optional, default is active problem)

Example: 
```sh
cop problem toggle-solved -i 3 
```
(Toggle the solved state for problem with ID 3)

#### Toggle Editorial

Toggle the solved editorial of the given problem.

Input:
1. `-i` or `--id`: ID of the problem (Optional, default is active problem)

Example: 
```sh
cop problem toggle-editorial -i 3 
```
(Toggle the editorial state for problem with ID 3)

#### Watch

Start watching for file changes given problem. Once a file changes, recompile the appropriate file,
and reevaluate on the given sample test cases.

Inputs:

1. `-i` or `--id`: ID of contest (Optional, default is activate problem)

Example: 
```sh
cop contest watch -i 3 
```


#### Delete

Delete given problem

Inputs:

1. ID of contest to delete

Example: 
``` cop
contest delete 3 
```
(Delete Problem with ID 3)

## Hooks

You can run custom programs before / after `cop` commands using hooks. To get started, create
`post-contest-create-hook.sh` inside the config directory (`~/.config/cop/` in GNU / Linux):

```sh
echo "POST CONTEST CREATE HOOK"
```

Now, next time you run `cop contest create`, this script will be run. The output of the shell
script is also shown.

List of all possible hooks:

1. `pre-contest-create-hook.sh`
2. `post-contest-create-hook.sh`
3. `pre-contest-activate-hook.sh`
4. `post-contest-activate-hook.sh`
5. `pre-contest-set-solved-hook.sh`
6. `post-contest-set-solved-hook.sh`
7. `pre-contest-delete-hook.sh`
8. `post-contest-delete-hook.sh`
9. `post-contest-watch-file-changed-hook.sh`
10. `pre-problem-create-hook.sh`
11. `post-problem-create-hook.sh`
12. `pre-problem-toggle-solved-hook.sh`
13. `post-problem-toggle-solved-hook.sh`
14. `pre-problem-toggle-editorial-hook.sh`
15. `post-problem-toggle-editorial-hook.sh`
16. `pre-problem-delete-hook.sh`
17. `post-problem-delete-hook.sh`
18. `post-problem-watch-file-changed-hook.sh`
