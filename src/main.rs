use chrono::prelude::*;
use lib::db::db_config;
use shellexpand;
use structopt::StructOpt;

mod lib;
use lib::helper::try_parse_datetime_from_string;

#[derive(Debug, StructOpt)]
#[structopt(name = "Cop", about = "A helper program for Competitive Program")]
struct Cop {
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Debug, StructOpt)]
enum Command {
    /// Contest Commands
    #[structopt(alias = "c")]
    Contest(ContestCommand),

    /// Problem Commands
    #[structopt(alias = "p")]
    Problem(ProblemCommand),
}

#[derive(Debug, StructOpt)]
enum ContestCommand {
    /// Create a Contest in "contest_dir"
    #[structopt(alias = "c")]
    Create {
        /// Name of the contest
        name: String,

        /// Platform on which the contest is being hosted on. Currently
        /// supported values are "CC", "CF"
        #[structopt(short, long)]
        platform: String,

        /// Number of Problems in the contest / Number of problems you want to
        /// solve in the contest
        #[structopt(short, long)]
        no_problem: u8,

        /// Time and Date of the contest. Give in "hh:mm dd/mm/yyyy" format
        #[structopt(short, long, parse(try_from_str = try_parse_datetime_from_string))]
        date: DateTime<Local>,

        /// Set Activate Contest
        #[structopt(short, long)]
        active: bool,
    },

    /// List all contests created by me
    #[structopt(alias = "l")]
    List {
        /// Filter using Regex String .
        #[structopt(short, long)]
        regex: Option<String>,

        /// Filter contests after this date. Give in "hh:mm dd/mm/yyyy" format
        #[structopt(short, long, parse(try_from_str = try_parse_datetime_from_string))]
        start_date: Option<DateTime<Local>>,

        /// Filter contests before this date. Give in "hh:mm dd/mm/yyyy" format
        #[structopt(short, long, parse(try_from_str = try_parse_datetime_from_string))]
        end_date: Option<DateTime<Local>>,
    },

    /// Activate Given Contest
    #[structopt(alias = "a")]
    Activate {
        /// Contest ID to Activate
        id: u32,

        /// Problem ID to activate
        #[structopt(short, long)]
        problem_id: Option<u8>,
    },

    /// Set Number of Problems Solved
    #[structopt(alias = "s")]
    SetSolved {
        /// Number of problems solved
        no_probs_solved: u8,

        /// ID of Contest. Will default to active contest
        #[structopt(short, long)]
        id_contest: Option<u32>,
    },

    /// Start Watching Contest
    #[structopt(alias = "w")]
    Watch {
        /// Contest ID to watch
        #[structopt(short, long)]
        id: Option<u32>,
    },

    /// Compile and Run a Problem
    #[structopt(alias = "r")]
    Run {
        /// Contest ID to run
        #[structopt(short, long)]
        id: Option<u32>,

        /// Problem ID to run
        #[structopt(short, long)]
        problem_id: Option<u8>,
    },

    /// Delete Given Contest
    #[structopt(alias = "d")]
    Delete {
        /// Contest ID to Delete
        id: u32,
    },
}

#[derive(Debug, StructOpt)]
enum ProblemCommand {
    /// Create Problem in "problem_dir"
    #[structopt(alias = "c")]
    Create {
        /// Name of Problem
        name: String,

        /// Platform on which the problem is hosted on
        #[structopt(short, long)]
        platform: String,

        /// Set Activate Contest
        #[structopt(short, long)]
        active: bool,
    },

    /// List Problems
    #[structopt(alias = "l")]
    List {
        /// Filter using Regex String .
        #[structopt(short, long)]
        regex: Option<String>,

        /// Filter solved problems
        #[structopt(short, long)]
        solved: bool,

        /// Filter unsolved problems
        #[structopt(short, long, conflicts_with = "solved")]
        unsolved: bool,

        /// Filter problems with editorial
        #[structopt(short, long)]
        editorial: bool,

        /// Filter problems without editorial
        #[structopt(short, long, conflicts_with = "editorial")]
        no_editorial: bool,
    },

    /// Toggle Problem Solved Status
    #[structopt(alias = "ts")]
    ToggleSolved {
        /// ID of problem
        #[structopt(short, long)]
        id: Option<u32>,
    },

    /// Toggle Problem Editorial Status
    #[structopt(alias = "te")]
    ToggleEditorial {
        /// ID of problem
        #[structopt(short, long)]
        id: Option<u32>,
    },

    /// Activate problem
    #[structopt(alias = "a")]
    Activate {
        /// ID of problem
        id: u32,
    },

    /// Run Problem
    #[structopt(alias = "r")]
    Run {
        /// ID of problem
        #[structopt(short, long)]
        id: Option<u32>,

        /// Run Executable only (do not compile)
        #[structopt(short, long)]
        run_only: bool,
    },

    /// Watch Problem
    #[structopt(alias = "w")]
    Watch {
        /// ID of problem
        #[structopt(short, long)]
        id: Option<u32>,
    },

    /// Delete Problem
    #[structopt(alias = "d")]
    Delete {
        /// ID of problem
        id: u32,
    },
}

fn main() -> Result<(), confy::ConfyError> {
    let mut conf: lib::config::MyConfig = confy::load("cop")?;
    conf.contest_dir = shellexpand::tilde(&conf.contest_dir).to_string();
    conf.problem_dir = shellexpand::tilde(&conf.problem_dir).to_string();
    conf.template_path = shellexpand::tilde(&conf.template_path).to_string();

    let db_conn = db_config();

    match Cop::from_args().cmd {
        Command::Contest(ccmd) => match ccmd {
            ContestCommand::Create {
                name,
                platform,
                no_problem,
                date,
                active,
            } => {
                let contest_id = lib::contest::create_contest(
                    &conf,
                    &db_conn,
                    lib::contest::Contest {
                        id: 0,
                        name,
                        platform,
                        no_problem,
                        date,
                        solved: 0,
                    },
                );
                if active {
                    lib::contest::activate_contest(&mut conf, &db_conn, contest_id, Some(1));
                }
            }
            ContestCommand::List {
                regex,
                start_date,
                end_date,
            } => lib::contest::list_contest(&conf, &db_conn, regex, start_date, end_date),
            ContestCommand::Activate { id, problem_id } => {
                lib::contest::activate_contest(&mut conf, &db_conn, id, problem_id)
            }
            ContestCommand::SetSolved {
                no_probs_solved,
                id_contest,
            } => lib::contest::set_solved(&conf, &db_conn, no_probs_solved, id_contest),
            ContestCommand::Delete { id } => lib::contest::delete_contest(&conf, &db_conn, id),
            ContestCommand::Run { id, problem_id } => {
                lib::contest::run_contest(&conf, &db_conn, id, problem_id, None)
            }
            ContestCommand::Watch { id } => lib::contest::watch_contest(&conf, &db_conn, id),
        },
        Command::Problem(pcmd) => match pcmd {
            ProblemCommand::Create {
                name,
                platform,
                active,
            } => {
                let problem_id = lib::problem::create_problem(
                    &conf,
                    &db_conn,
                    lib::problem::Problem {
                        id: 0,
                        name,
                        platform,
                        solved: 0,
                        has_editorial: 0,
                    },
                );
                if active {
                    lib::problem::activate_problem(&mut conf, &db_conn, problem_id);
                }
            }
            ProblemCommand::List {
                regex,
                solved,
                unsolved,
                editorial,
                no_editorial,
            } => lib::problem::list_problem(
                &conf,
                &db_conn,
                regex,
                solved,
                unsolved,
                editorial,
                no_editorial,
            ),
            ProblemCommand::ToggleSolved { id } => {
                lib::problem::toggle_solved_problem(&conf, &db_conn, id)
            }
            ProblemCommand::ToggleEditorial { id } => {
                lib::problem::toggle_editorial_problem(&conf, &db_conn, id)
            }
            ProblemCommand::Activate { id } => {
                lib::problem::activate_problem(&mut conf, &db_conn, id)
            }
            ProblemCommand::Run { id, run_only } => {
                lib::problem::run_problem(&conf, &db_conn, id, !run_only, None)
            }
            ProblemCommand::Watch { id } => lib::problem::watch_problem(&conf, &db_conn, id),
            ProblemCommand::Delete { id } => lib::problem::delete_problem(&conf, &db_conn, id),
        },
    }
    Ok(())
}
