use std::{
    fs,
    io::{self, Read, Write},
    path::PathBuf,
    process, sync,
};

use chrono::prelude::*;
use colored::*;
use indicatif;
use notify::{PollWatcher, RecursiveMode, Watcher};
use regex::Regex;
use rusqlite::Connection;
use similar::{ChangeTag, TextDiff};
use tabled::Style;

use super::{
    config::MyConfig,
    db::{
        delete_contest_db, get_contest_by_id, get_list_contest_db, insert_contest_db,
        set_solved_contest_db,
    },
    helper::{get_compile_command, get_contest_dir_from_contest, get_run_command},
    hooks::{
        post_contest_activate_hook, post_contest_create_hook, post_contest_delete_hook,
        post_contest_set_solved_hook, post_contest_watch_file_changed_hook,
        pre_contest_activate_hook, pre_contest_create_hook, pre_contest_delete_hook,
        pre_contest_set_solved_hook,
    },
};

#[derive(Debug, Clone)]
pub struct Contest {
    pub id: u32,
    pub name: String,
    pub platform: String,
    pub no_problem: u8,
    pub date: DateTime<Local>,
    pub solved: u8,
}

pub fn create_contest(conf: &MyConfig, db_conn: &Connection, contest: Contest) -> u32 {
    let contest_dir = &conf.contest_dir;

    pre_contest_create_hook(&contest);

    let no_problem = contest.no_problem.clone();
    let contest_dir_name = get_contest_dir_from_contest(&contest);
    let contest_root_path = format!("{contest_dir}/{contest_dir_name}");

    println!(
        "Creating Contest Directory: {} ...",
        contest_root_path.green().bold()
    );

    fs::create_dir(&contest_root_path).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Failed To Create Contest Directory: {e}")
                .red()
                .bold()
        );
        process::exit(1);
    });

    println!("{}", "Created Contest Directory!\n".green().bold());

    let bar = indicatif::ProgressBar::new(5 * (no_problem as u64));
    bar.set_style(
        indicatif::ProgressStyle::default_bar()
            .template("{bar:40.cyan/blue} {pos:>7}/{len:7} {msg}"),
    );

    for problem_id in 1..=no_problem {
        let problem_path = format!("{contest_root_path}/p{problem_id}");

        bar.set_message(format!("Creating Problem Directory"));

        fs::create_dir(&problem_path).unwrap_or_else(|e| {
            bar.abandon_with_message(format!(
                "{}",
                format!("Failed To Create Problem {} Directory: {}", problem_id, e)
                    .red()
                    .bold(),
            ));
            process::exit(1);
        });

        bar.inc(1);
        bar.set_message("Copying Template");

        fs::copy(&conf.template_path, format!("{problem_path}/main.cpp")).unwrap_or_else(|e| {
            bar.abandon_with_message(format!(
                "{}",
                format!("Failed To Copy Template: {e}").red().bold(),
            ));
            process::exit(1);
        });

        bar.inc(1);
        bar.set_message("Creating Problem Input File");

        fs::File::create(format!("{problem_path}/problem.in")).unwrap_or_else(|e| {
            bar.abandon_with_message(format!(
                "{}",
                format!("Failed To Create Problem Input File: {e}")
                    .red()
                    .bold(),
            ));
            process::exit(1);
        });

        bar.inc(1);
        bar.set_message("Creating Problem Output File");

        fs::File::create(format!("{problem_path}/problem.out")).unwrap_or_else(|e| {
            bar.abandon_with_message(format!(
                "{}",
                format!("Failed To Create Problem Output File: {e}")
                    .red()
                    .bold(),
            ));
            process::exit(1);
        });

        bar.inc(1);
        bar.set_message("Creating Program Output File");

        fs::File::create(format!("{problem_path}/program.out")).unwrap_or_else(|e| {
            bar.abandon_with_message(format!(
                "{}",
                format!("Failed To Create Program Output File: {e}")
                    .red()
                    .bold(),
            ));
            process::exit(1);
        });
        bar.inc(1);
    }

    bar.finish_with_message(format!(
        "{}",
        format!("Problem Setup Complete").green().bold()
    ));

    println!("");

    println!("{}", "Saving Contest to DB ...");
    let contest_id: u32 = insert_contest_db(db_conn, &contest);
    println!("{}", "Saved Contest to DB\n".green().bold());

    post_contest_create_hook(contest_id, &contest, contest_root_path);

    contest_id
}

pub fn list_contest(
    conf: &MyConfig,
    db_conn: &Connection,
    regex: Option<String>,
    start_date: Option<DateTime<Local>>,
    end_date: Option<DateTime<Local>>,
) {
    let rg: Regex;
    if let Some(regex_str) = &regex {
        rg = Regex::new(&regex_str).unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while evaluating regex: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
    } else {
        rg = Regex::new("(.*?)").unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while evaluating default regex: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
    }

    let table_heading = vec![vec![
        "ID".to_string(),
        "Name".to_string(),
        "Platform".to_string(),
        "Date".to_string(),
        "No. Solved".to_string(),
    ]];

    let contests_vec: Vec<Vec<String>> = get_list_contest_db(db_conn)
        .iter()
        .filter(|c| rg.is_match(&c.name))
        .filter(|c| {
            if let Some(dt) = start_date {
                c.date >= dt
            } else {
                true
            }
        })
        .filter(|c| {
            if let Some(dt) = end_date {
                c.date <= dt
            } else {
                true
            }
        })
        .map(|c| {
            let date_string = format!(
                "{:02}:{:02}  {:02}/{:02}/{:04}",
                c.date.hour(),
                c.date.minute(),
                c.date.day(),
                c.date.month(),
                c.date.year()
            );
            let no_solved_string = format!("{} / {}", c.solved, c.no_problem);
            let mut pb = String::new();
            let expected_fill = ((c.solved as f64) / (c.no_problem as f64)) * (15 as f64);
            for i in 1..=15 {
                let diff = i as f64 - expected_fill;
                if i as f64 <= expected_fill {
                    pb.push('█');
                } else if diff >= 1f64 {
                    pb.push(' ');
                } else if diff >= 7f64 / 8f64 {
                    pb.push('▉');
                } else if diff >= 6f64 / 8f64 {
                    pb.push('▊');
                } else if diff >= 5f64 / 8f64 {
                    pb.push('▋');
                } else if diff >= 4f64 / 8f64 {
                    pb.push('▌');
                } else if diff >= 3f64 / 8f64 {
                    pb.push('▍');
                } else if diff >= 2f64 / 8f64 {
                    pb.push('▎');
                } else if diff >= 1f64 / 8f64 {
                    pb.push('▏');
                } else {
                    pb.push(' ');
                }
            }
            let no_solved_string =
                no_solved_string + "  " + pb.on_black().green().to_string().as_str();

            if conf.active_contest_id == c.id {
                vec![
                    c.id.to_string().green().bold().to_string(),
                    c.name.green().bold().to_string(),
                    c.platform.green().bold().to_string(),
                    date_string.green().bold().to_string(),
                    no_solved_string.to_string().green().bold().to_string(),
                ]
            } else {
                vec![
                    c.id.to_string(),
                    c.name.clone(),
                    c.platform.clone(),
                    date_string,
                    no_solved_string.to_string(),
                ]
            }
        })
        .collect();

    let table_data: Vec<Vec<String>> = table_heading
        .iter()
        .chain(contests_vec.iter())
        .cloned()
        .collect();

    println!(
        "{}",
        tabled::builder::Builder::from_iter(table_data)
            .build()
            .with(Style::modern())
    );
}

pub fn activate_contest(
    conf: &mut MyConfig,
    db_conn: &Connection,
    id: u32,
    problem_id: Option<u8>,
) {
    let contest = get_contest_by_id(db_conn, id).unwrap_or_else(|| {
        println!(
            "{}",
            "Error occurred while getting Contest By ID. Is the Contest ID valid?"
                .red()
                .bold()
        );
        process::exit(1);
    });

    pre_contest_activate_hook(&contest);

    println!("Setting Active Contest ...");

    if let Some(pid) = problem_id {
        if contest.no_problem < pid {
            let np = contest.no_problem;
            println!("{}", format!("Problem ID to big. Trying to set problem number {pid} to be active, but contest has only {np} problems").red().bold());
            process::exit(1);
        }
    }

    conf.active_contest_id = contest.id;
    confy::store("cop", conf).unwrap_or_else(|e| {
        println!("Could not store Config: {e}");
        process::exit(1);
    });

    println!("{}", "Set Active Contest\n".green().bold());

    post_contest_activate_hook(&contest);
}

pub fn set_solved(conf: &MyConfig, db_conn: &Connection, no_solved: u8, id: Option<u32>) {
    let contest = get_contest_by_id(db_conn, id.unwrap_or_else(|| conf.active_contest_id))
        .unwrap_or_else(|| {
            println!("{}", "Could not get Contest By ID".red().bold());
            process::exit(1);
        });

    pre_contest_set_solved_hook(&contest);

    println!("Setting Number of Problems Solved ...");

    if contest.no_problem < no_solved {
        println!(
            "{}",
            "Number of problems in contest is less than number of problems you have solved!"
                .red()
                .bold()
        );
        process::exit(1);
    }

    set_solved_contest_db(db_conn, contest.id, no_solved);

    println!("{}", "Set Number of Problems Solved!\n".green().bold());

    post_contest_set_solved_hook(&contest);
}

pub fn delete_contest(conf: &MyConfig, db_conn: &Connection, id: u32) {
    let contests_dir = conf.contest_dir.clone();
    let contest = get_contest_by_id(db_conn, id).unwrap_or_else(|| {
        println!("{}", "Could not get Contest By ID".red().bold());
        process::exit(1);
    });

    pre_contest_delete_hook(&contest);

    println!("{}", "Deleting Contest Directory ...");
    let contest_dir = get_contest_dir_from_contest(&contest);
    let dir_delete = format!("{contests_dir}/{contest_dir}");
    fs::remove_dir_all(dir_delete).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Could not delete Contest Directory: {e}")
                .red()
                .bold()
        );
        process::exit(1);
    });
    println!("{}", "Deleted Contest Directory!\n".green().bold());

    println!("{}", "Deleting Contest from DB ...");
    delete_contest_db(db_conn, id);
    println!("{}", "Deleted Contest from DB!\n".green().bold());

    post_contest_delete_hook(&contest);
}

pub fn run_contest(
    conf: &MyConfig,
    db_conn: &Connection,
    id: Option<u32>,
    problem_id: Option<u8>,
    contest: Option<Contest>,
) {
    let problem_id = problem_id.unwrap_or_else(|| 1);

    let contest = contest.unwrap_or_else(|| {
        get_contest_by_id(db_conn, id.unwrap_or_else(|| conf.active_contest_id)).unwrap_or_else(
            || {
                println!("{}", "Could not get Contest By ID".red().bold());
                process::exit(1);
            },
        )
    });

    let contest_dir = &conf.contest_dir;
    let contest_dir_name = get_contest_dir_from_contest(&contest);
    let problem_dir_path_str = format!("{contest_dir}/{contest_dir_name}/p{problem_id}");

    let compile_cmd = get_compile_command(&problem_dir_path_str);

    io::stdout().write_all(&compile_cmd.stdout).unwrap();
    io::stderr().write_all(&compile_cmd.stderr).unwrap();
    if !compile_cmd.status.success() {
        println!(
            "{}\n",
            "Compiler Error! Fix the bug and try again!".red().bold()
        );
        return;
    }

    println!("{}", "Compiling Successful".green().bold());

    let problem_in_path = format!("{problem_dir_path_str}/problem.in");
    let problem_out_path = format!("{problem_dir_path_str}/problem.out");
    let program_out_path = format!("{problem_dir_path_str}/program.out");

    let run_cmd = get_run_command(&problem_dir_path_str, &problem_in_path, &program_out_path);

    io::stdout().write_all(&run_cmd.stdout).unwrap();
    io::stderr().write_all(&run_cmd.stderr).unwrap();
    if !run_cmd.status.success() {
        println!(
            "{}\n",
            "Runtime Error! Fix the bug and try again!".red().bold()
        );
        return;
    }
    println!("{}\n", "Run Successful!".green().bold());

    let mut problem_in = String::new();
    let mut problem_in_file =
        fs::File::open(problem_in_path).expect("Could not open Problem In File");
    problem_in_file
        .read_to_string(&mut problem_in)
        .expect("Could not read Problem In File");

    let mut problem_out = String::new();
    let mut problem_out_file =
        fs::File::open(problem_out_path).expect("Could not open Problem Out File");
    problem_out_file
        .read_to_string(&mut problem_out)
        .expect("Could not read Problem Out File");

    let mut program_out = String::new();
    let mut program_out_file =
        fs::File::open(program_out_path).expect("Could not open Program Out File");
    program_out_file
        .read_to_string(&mut program_out)
        .expect("Could not read Program Out File");

    println!("Input:");
    println!("-----------------------");
    println!("{problem_in}");

    println!("Expected Output:");
    println!("-----------------------");
    println!("{problem_out}");

    println!("Program Output:");
    println!("-----------------------");
    println!("{program_out}");

    let diff = TextDiff::from_lines(program_out.as_str(), problem_out.as_str());
    println!("Diff:");
    println!("-----------------------");
    for change in diff.iter_all_changes() {
        match change.tag() {
            ChangeTag::Delete => {
                print!("{}", format!("-{change}").bold().on_truecolor(130, 50, 50));
            }
            ChangeTag::Insert => {
                print!("{}", format!("+{change}").bold().on_truecolor(50, 130, 50));
            }
            ChangeTag::Equal => {
                print!("{}", format!("{change}"));
            }
        };
    }
    println!("");
    if problem_out == program_out {
        println!("{}", format!("✔ All Good!").green().bold())
    } else {
        println!("{}", format!("❌ Not Good!").red().bold())
    }
}

pub fn watch_contest(conf: &MyConfig, db_conn: &Connection, id: Option<u32>) {
    let contests_dir = conf.contest_dir.clone();
    let contest = get_contest_by_id(db_conn, id.unwrap_or_else(|| conf.active_contest_id))
        .unwrap_or_else(|| {
            println!("{}", "Could not get Contest By ID".red().bold());
            process::exit(1);
        });

    let contest_dir_name = get_contest_dir_from_contest(&contest);
    let (tx, rx) = sync::mpsc::channel();
    let mut contest_watcher = PollWatcher::with_delay_ms(tx, 1000).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Cannot Create Poll Watcher: {e}").red().bold()
        );
        process::exit(1);
    });

    contest_watcher
        .watch(
            format!("{contests_dir}/{contest_dir_name}"),
            RecursiveMode::Recursive,
        )
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Cannot Create Poll Watcher: {e}").red().bold()
            );
            process::exit(1);
        });

    println!("{}", "Started Watching Contest!\n".green().bold());

    loop {
        match rx.recv() {
            Ok(event) => {
                if event.path.clone() == None {
                    println!("{}", "No Path in Event!".red().bold());
                    continue;
                }
                let path: PathBuf = event.path.as_ref().unwrap().to_path_buf();

                if path.is_dir() {
                    continue;
                }

                match event.op {
                    Ok(_) => {
                        let file_name = path.file_name().unwrap().to_str().unwrap();
                        if file_name != "main.cpp"
                            && file_name != "problem.in"
                            && file_name != "problem.out"
                        {
                            continue;
                        }
                        let mut problem_dir_path = path.clone();
                        problem_dir_path.pop();
                        let problem_name = problem_dir_path.file_name().unwrap().to_str().unwrap();
                        let problem_id: u8 = problem_name[1..].parse().unwrap();
                        println!("Problem {problem_id}'s {file_name} changed. Recompiling and testing ...");

                        run_contest(&conf, &db_conn, id, Some(problem_id), Some(contest.clone()));

                        post_contest_watch_file_changed_hook(
                            &contest,
                            problem_id,
                            file_name.to_string(),
                        );
                    }
                    Err(err) => {
                        println!(
                            "{}",
                            format!("Error occurred during watch: {err}").red().bold()
                        );
                        continue;
                    }
                }
            }
            Err(e) => {
                println!(
                    "{}",
                    format!("An Error occurred during watch: {e}").red().bold()
                );
            }
        }
    }
}
