use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct MyConfig {
    pub contest_dir: String,
    pub problem_dir: String,
    pub template_path: String,
    pub active_problem_id: u32,
    pub active_contest_id: u32,
}

impl Default for MyConfig {
    fn default() -> Self {
        MyConfig {
            contest_dir: "".to_string(),
            problem_dir: "".to_string(),
            template_path: "".to_string(),
            active_problem_id: 0,
            active_contest_id: 0,
        }
    }
}
