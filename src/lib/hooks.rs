use chrono::{Datelike, Timelike};
use colored::Colorize;
use directories::ProjectDirs;

use super::{contest::Contest, problem::Problem};
use std::{
    io::{self, Write},
    path::PathBuf,
    process::{self, Command},
};

pub fn create_hook(pre_post: &str, hook_name: &str, cmd_args: Vec<String>) {
    let config_dir: PathBuf;
    if let Some(project_dirs) = ProjectDirs::from("com", "cop", "cop") {
        config_dir = project_dirs.config_dir().to_path_buf();
    } else {
        println!("{}", "Could Not Get Data Directory of OS".red().bold());
        process::exit(1);
    }

    println!("Executing {pre_post} {hook_name} Hook ...");

    let name_shell_script = pre_post.to_lowercase().clone()
        + "-"
        + hook_name.to_lowercase().replace(" ", "-").clone().as_str()
        + "-hook";

    let mut shell_script = config_dir.clone();
    shell_script.push(name_shell_script);
    shell_script.set_extension("sh");

    if !shell_script.exists() {
        println!("Could not find {pre_post} {hook_name} Hook\n");
        return;
    }

    let cmd = Command::new("sh")
        .arg(shell_script.to_str().unwrap())
        .args(cmd_args)
        .output()
        .expect("Failed to execute Pre Contest Create Hook");
    io::stdout().write_all(&cmd.stdout).unwrap();
    io::stderr().write_all(&cmd.stderr).unwrap();

    if !cmd.status.success() {
        println!(
            "{}",
            format!("Failed to execute {pre_post} {hook_name} Hook")
                .red()
                .bold()
        );
        process::exit(1);
    }
    println!(
        "{}",
        format!("Executed {pre_post} {hook_name} Hook\n")
            .green()
            .bold()
    );
}

pub fn pre_contest_create_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );

    let cmd_args = vec![
        contest.name.to_string(),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
    ];
    create_hook("Pre", "Contest Create", cmd_args);
}

pub fn post_contest_create_hook(contest_id: u32, contest: &Contest, contest_path: String) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest_id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest_path,
    ];

    create_hook("Post", "Contest Create", cmd_args);
}

pub fn pre_contest_activate_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
    ];

    create_hook("Pre", "Contest Activate", cmd_args);
}

pub fn post_contest_activate_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
    ];

    create_hook("Post", "Contest Activate", cmd_args);
}

pub fn pre_contest_set_solved_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
    ];

    create_hook("Pre", "Contest Set Solved", cmd_args);
}

pub fn post_contest_set_solved_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
    ];

    create_hook("Post", "Contest Set Solved", cmd_args);
}

pub fn pre_contest_delete_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
    ];

    create_hook("Pre", "Contest Delete", cmd_args);
}

pub fn post_contest_delete_hook(contest: &Contest) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
    ];

    create_hook("Post", "Contest Delete", cmd_args);
}

pub fn post_contest_watch_file_changed_hook(
    contest: &Contest,
    problem_id: u8,
    file_changed: String,
) {
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        contest.date.hour(),
        contest.date.minute(),
        contest.date.day(),
        contest.date.month(),
        contest.date.year()
    );
    let cmd_args = vec![
        contest.id.to_string(),
        contest.name.replace(" ", "-").replace("/", "-"),
        contest.platform.clone(),
        contest.no_problem.to_string(),
        date_string,
        contest.solved.to_string(),
        problem_id.to_string(),
        file_changed,
    ];

    create_hook("Post", "Contest Watch File Changed", cmd_args);
}

pub fn pre_problem_create_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.name.replace(" ", "-").replace("/", "-"),
        problem.platform.clone(),
    ];

    create_hook("Pre", "Problem Create", cmd_args);
}

pub fn post_problem_create_hook(problem_id: u32, problem: &Problem, problem_dir: String) {
    let cmd_args = vec![
        problem_id.to_string(),
        problem.name.replace(" ", "-").replace("/", "-"),
        problem.platform.clone(),
        problem_dir,
    ];

    create_hook("Post", "Problem Create", cmd_args);
}

pub fn pre_problem_toggle_solved_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Pre", "Problem Toggle Solved", cmd_args);
}

pub fn post_problem_toggle_solved_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Post", "Problem Toggle Solved", cmd_args);
}

pub fn pre_problem_toggle_editorial_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Pre", "Problem Toggle Editorial", cmd_args);
}

pub fn post_problem_toggle_editorial_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Post", "Problem Toggle Editorial", cmd_args);
}

pub fn pre_problem_activate_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Pre", "Problem Activate", cmd_args);
}

pub fn post_problem_activate_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Post", "Problem Activate", cmd_args);
}

pub fn post_problem_watch_file_changed_hook(problem: &Problem, file_name: String) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
        file_name,
    ];

    create_hook("Post", "Problem Watch File Changed", cmd_args);
}

pub fn pre_problem_delete_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Pre", "Problem Delete", cmd_args);
}

pub fn post_problem_delete_hook(problem: &Problem) {
    let cmd_args = vec![
        problem.id.to_string(),
        problem.name.clone(),
        problem.platform.clone(),
        problem.solved.to_string(),
        problem.has_editorial.to_string(),
    ];

    create_hook("Post", "Problem Delete", cmd_args);
}
