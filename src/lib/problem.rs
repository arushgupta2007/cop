use std::{
    fs,
    io::{self, Read, Write},
    path::PathBuf,
    process, sync,
};

use colored::Colorize;
use notify::{PollWatcher, RecursiveMode, Watcher};
use regex::Regex;
use rusqlite::Connection;
use similar::{ChangeTag, TextDiff};
use tabled::Style;

use crate::lib::helper::{get_compile_command, get_run_command};

use super::{
    config::MyConfig,
    db::{
        delete_problem_db, get_list_problem_db, get_problem_by_id, insert_problem_db,
        toggle_has_editorial_problem_by_id, toggle_solved_problem_by_id,
    },
    helper::get_problem_dir_from_problem,
    hooks::{
        post_problem_activate_hook, post_problem_create_hook, post_problem_delete_hook,
        post_problem_toggle_editorial_hook, post_problem_toggle_solved_hook,
        post_problem_watch_file_changed_hook, pre_problem_activate_hook, pre_problem_create_hook,
        pre_problem_delete_hook, pre_problem_toggle_editorial_hook, pre_problem_toggle_solved_hook,
    },
};

fn display_bool_in_table(i: &u8) -> String {
    match i {
        0 => " ❌".bold().to_string(),
        1 => "✔".bold().to_string(),
        _ => unreachable!(),
    }
}

#[derive(Debug, Clone)]
pub struct Problem {
    pub id: u32,
    pub name: String,
    pub platform: String,
    pub solved: u8,
    pub has_editorial: u8,
}

pub fn create_problem(conf: &MyConfig, db_conn: &Connection, problem: Problem) -> u32 {
    let problem_dir = &conf.problem_dir;

    pre_problem_create_hook(&problem);

    let problem_dir_name = get_problem_dir_from_problem(&problem);
    let problem_root_path = format!("{problem_dir}/{problem_dir_name}");

    println!("Creating Problem Directory ...");
    fs::create_dir(&problem_root_path).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Failed To Create Problem Directory: {e}")
                .red()
                .bold()
        );
        process::exit(1);
    });
    println!("{}", "Created Problem Directory!\n".green().bold());

    println!("Copying Template ...");
    fs::copy(&conf.template_path, format!("{problem_root_path}/main.cpp")).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("{}", format!("Failed To Copy Template: {e}").red().bold(),)
        );
        process::exit(1);
    });
    println!("{}", "Copied Template!\n".green().bold());

    println!("Creating Problem Input File ...");
    fs::File::create(format!("{problem_root_path}/problem.in")).unwrap_or_else(|e| {
        println!(
            "{}",
            format!(
                "{}",
                format!("Failed To Create Problem Input File: {e}")
                    .red()
                    .bold(),
            )
        );
        process::exit(1);
    });
    println!("{}", "Created Problem Input File!\n".green().bold());

    println!("Creating Problem Output File ...");
    fs::File::create(format!("{problem_root_path}/problem.out")).unwrap_or_else(|e| {
        println!(
            "{}",
            format!(
                "{}",
                format!("Failed To Create Problem Output File: {e}")
                    .red()
                    .bold(),
            )
        );
        process::exit(1);
    });
    println!("{}", "Created Problem Output File!\n".green().bold());

    println!("Creating Program Output File ...");
    fs::File::create(format!("{problem_root_path}/program.out")).unwrap_or_else(|e| {
        println!(
            "{}",
            format!(
                "{}",
                format!("Failed To Create Program Output File: {e}")
                    .red()
                    .bold(),
            )
        );
        process::exit(1);
    });
    println!("{}", "Created Problem Output File!\n".green().bold());

    println!("Creating Question File ...");
    fs::File::create(format!("{problem_root_path}/question.md")).unwrap_or_else(|e| {
        println!(
            "{}",
            format!(
                "{}",
                format!("Failed To Create Question File: {e}").red().bold(),
            )
        );
        process::exit(1);
    });
    println!("{}", "Created Question File!\n".green().bold());

    println!("Creating Editorial File ...");
    fs::File::create(format!("{problem_root_path}/editorial.md")).unwrap_or_else(|e| {
        println!(
            "{}",
            format!(
                "{}",
                format!("Failed To Create Editorial File: {e}").red().bold(),
            )
        );
        process::exit(1);
    });
    println!("{}", "Created Editorial File!\n".green().bold());

    println!("Saving Problem to DB ...");
    let problem_id = insert_problem_db(db_conn, &problem);
    println!("{}", "Saved Problem to DB!\n".green().bold());

    post_problem_create_hook(problem_id, &problem, problem_root_path.clone());

    problem_id
}

pub fn list_problem(
    conf: &MyConfig,
    db_conn: &Connection,
    regex: Option<String>,
    solved: bool,
    unsolved: bool,
    editorial: bool,
    no_editorial: bool,
) {
    let rg: Regex;
    if let Some(regex_str) = &regex {
        rg = Regex::new(&regex_str).unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while evaluating regex: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
    } else {
        rg = Regex::new("(.*?)").unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while evaluating default regex: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
    }

    let table_heading = vec![vec![
        "ID".to_string(),
        "Name".to_string(),
        "Platform".to_string(),
        "Solved".to_string(),
        "Editorial".to_string(),
    ]];

    let problems_vec: Vec<Vec<String>> = get_list_problem_db(db_conn)
        .iter()
        .filter(|p| rg.is_match(&p.name))
        .filter(|p| {
            if solved {
                p.solved == 1
            } else if unsolved {
                p.solved == 0
            } else {
                true
            }
        })
        .filter(|p| {
            if editorial {
                p.has_editorial == 1
            } else if no_editorial {
                p.has_editorial == 0
            } else {
                true
            }
        })
        .map(|p| {
            if conf.active_problem_id == p.id {
                vec![
                    p.id.to_string().green().bold().to_string(),
                    p.name.green().bold().to_string(),
                    p.platform.green().bold().to_string(),
                    display_bool_in_table(&p.solved).green().bold().to_string(),
                    display_bool_in_table(&p.has_editorial)
                        .green()
                        .bold()
                        .to_string(),
                ]
            } else {
                vec![
                    p.id.to_string(),
                    p.name.clone(),
                    p.platform.clone(),
                    display_bool_in_table(&p.solved),
                    display_bool_in_table(&p.has_editorial),
                ]
            }
        })
        .collect();

    let table_data: Vec<Vec<String>> = table_heading
        .iter()
        .chain(problems_vec.iter())
        .cloned()
        .collect();

    println!(
        "{}",
        tabled::builder::Builder::from_iter(table_data)
            .build()
            .with(Style::modern())
    );
}

pub fn toggle_solved_problem(conf: &MyConfig, db_conn: &Connection, id: Option<u32>) {
    let id = id.unwrap_or_else(|| conf.active_problem_id);
    let problem = get_problem_by_id(db_conn, id).unwrap_or_else(|| {
        println!(
            "{}",
            "Error occurred while getting Problem By ID. Is the Problem ID valid?"
                .red()
                .bold()
        );
        process::exit(1);
    });

    pre_problem_toggle_solved_hook(&problem);

    println!("Toggling Solved State ...");
    toggle_solved_problem_by_id(db_conn, id, problem.solved);
    println!("{}", "Toggled Solved State!\n".green().bold());

    post_problem_toggle_solved_hook(&problem);
}

pub fn toggle_editorial_problem(conf: &MyConfig, db_conn: &Connection, id: Option<u32>) {
    let id = id.unwrap_or_else(|| conf.active_problem_id);
    let problem = get_problem_by_id(db_conn, id).unwrap_or_else(|| {
        println!(
            "{}",
            "Error occurred while getting Problem By ID. Is the Problem ID valid?"
                .red()
                .bold()
        );
        process::exit(1);
    });

    pre_problem_toggle_editorial_hook(&problem);

    println!("Toggling Editorial State ...");
    toggle_has_editorial_problem_by_id(db_conn, id, problem.has_editorial);
    println!("{}", "Toggled Editorial State!\n".green().bold());

    post_problem_toggle_editorial_hook(&problem);
}

pub fn activate_problem(conf: &mut MyConfig, db_conn: &Connection, id: u32) {
    let problem = get_problem_by_id(db_conn, id).unwrap_or_else(|| {
        println!(
            "{}",
            "Error occurred while getting Problem By ID. Is the Problem ID valid?"
                .red()
                .bold()
        );
        process::exit(1);
    });

    pre_problem_activate_hook(&problem);

    println!("Setting Active Problem ...");
    conf.active_problem_id = id;
    confy::store("cop", conf).unwrap_or_else(|e| {
        println!("Could not store Config: {e}");
        process::exit(1);
    });
    println!("{}", "Set Active Problem!\n".green().bold());

    post_problem_activate_hook(&problem);
}

pub fn run_problem(
    conf: &MyConfig,
    db_conn: &Connection,
    id: Option<u32>,
    compile: bool,
    problem: Option<Problem>,
) {
    let problems_dir = conf.problem_dir.clone();

    let id = id.unwrap_or_else(|| conf.active_problem_id);

    let problem = problem.unwrap_or_else(|| {
        get_problem_by_id(db_conn, id).unwrap_or_else(|| {
            println!("{}", "Could not get Problem By ID".red().bold());
            process::exit(1);
        })
    });

    let problem_dir_name = get_problem_dir_from_problem(&problem);
    let problem_dir_path_str = format!("{problems_dir}/{problem_dir_name}");

    if compile {
        let compile_cmd = get_compile_command(&problem_dir_path_str);

        io::stdout().write_all(&compile_cmd.stdout).unwrap();
        io::stderr().write_all(&compile_cmd.stderr).unwrap();
        if !compile_cmd.status.success() {
            println!(
                "{}\n",
                "Compiler Error! Fix the bug and try again!".red().bold()
            );
            return;
        }

        println!("{}", "Compiling Successful".green().bold());
    }

    let problem_in_path = format!("{problem_dir_path_str}/problem.in");
    let problem_out_path = format!("{problem_dir_path_str}/problem.out");
    let program_out_path = format!("{problem_dir_path_str}/program.out");

    let run_cmd = get_run_command(&problem_dir_path_str, &problem_in_path, &program_out_path);

    io::stdout().write_all(&run_cmd.stdout).unwrap();
    io::stderr().write_all(&run_cmd.stderr).unwrap();
    if !run_cmd.status.success() {
        println!(
            "{}\n",
            "Runtime Error! Fix the bug and try again!".red().bold()
        );
        return;
    }
    println!("{}\n", "Run Successful!".green().bold());

    let mut problem_in = String::new();
    let mut problem_in_file =
        fs::File::open(problem_in_path).expect("Could not open Problem In File");
    problem_in_file
        .read_to_string(&mut problem_in)
        .expect("Could not read Problem In File");

    let mut problem_out = String::new();
    let mut problem_out_file =
        fs::File::open(problem_out_path).expect("Could not open Problem Out File");
    problem_out_file
        .read_to_string(&mut problem_out)
        .expect("Could not read Problem Out File");

    let mut program_out = String::new();
    let mut program_out_file =
        fs::File::open(program_out_path).expect("Could not open Program Out File");
    program_out_file
        .read_to_string(&mut program_out)
        .expect("Could not read Program Out File");

    println!("Input:");
    println!("-----------------------");
    println!("{problem_in}");

    println!("Expected Output:");
    println!("-----------------------");
    println!("{problem_out}");

    println!("Program Output:");
    println!("-----------------------");
    println!("{program_out}");

    let diff = TextDiff::from_lines(program_out.as_str(), problem_out.as_str());
    println!("Diff:");
    println!("-----------------------");
    for change in diff.iter_all_changes() {
        match change.tag() {
            ChangeTag::Delete => {
                print!("{}", format!("-{change}").bold().on_truecolor(130, 50, 50));
            }
            ChangeTag::Insert => {
                print!("{}", format!("+{change}").bold().on_truecolor(50, 130, 50));
            }
            ChangeTag::Equal => {
                print!("{}", format!("{change}"));
            }
        };
    }
    println!("");
    if problem_out == program_out {
        println!("{}", format!("✔ All Good!").green().bold())
    } else {
        println!("{}", format!("❌ Not Good!").red().bold())
    }
}

pub fn watch_problem(conf: &MyConfig, db_conn: &Connection, id: Option<u32>) {
    let problems_dir = conf.problem_dir.clone();

    let id = id.unwrap_or_else(|| conf.active_problem_id);
    let problem = get_problem_by_id(db_conn, id).unwrap_or_else(|| {
        println!("{}", "Could not get Problem By ID".red().bold());
        process::exit(1);
    });

    let problem_dir_name = get_problem_dir_from_problem(&problem);
    let (tx, rx) = sync::mpsc::channel();
    let mut problem_watcher = PollWatcher::with_delay_ms(tx, 1000).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Cannot Create Poll Watcher: {e}").red().bold()
        );
        process::exit(1);
    });

    let problem_dir_path_str = format!("{problems_dir}/{problem_dir_name}");

    problem_watcher
        .watch(&problem_dir_path_str, RecursiveMode::Recursive)
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Cannot Create Poll Watcher: {e}").red().bold()
            );
            process::exit(1);
        });

    println!("{}", "Started Watching Problem!\n".green().bold());

    loop {
        match rx.recv() {
            Ok(event) => {
                if event.path.clone() == None {
                    println!("{}", "No Path in Event!".red().bold());
                    continue;
                }
                let path: PathBuf = event.path.as_ref().unwrap().to_path_buf();

                if path.is_dir() {
                    continue;
                }

                match event.op {
                    Ok(_) => {
                        let file_name = path.file_name().unwrap().to_str().unwrap();
                        if file_name != "main.cpp"
                            && file_name != "problem.in"
                            && file_name != "problem.out"
                        {
                            continue;
                        }

                        println!("Problem's {file_name} changed. Recompiling and testing ...");

                        run_problem(
                            conf,
                            db_conn,
                            Some(id),
                            file_name == "main.cpp",
                            Some(problem.clone()),
                        );

                        post_problem_watch_file_changed_hook(&problem, file_name.to_string());
                    }
                    Err(err) => {
                        println!(
                            "{}",
                            format!("Error occurred during watch: {err}").red().bold()
                        );
                        continue;
                    }
                }
            }
            Err(e) => {
                println!(
                    "{}",
                    format!("An Error occurred during watch: {e}").red().bold()
                );
            }
        }
    }
}

pub fn delete_problem(conf: &MyConfig, db_conn: &Connection, id: u32) {
    let problems_dir = conf.problem_dir.clone();
    let problem = get_problem_by_id(db_conn, id).unwrap_or_else(|| {
        println!("{}", "Could not get Problem By ID".red().bold());
        process::exit(1);
    });

    pre_problem_delete_hook(&problem);

    println!("{}", "Deleting Problem Directory ...");
    let problem_dir = get_problem_dir_from_problem(&problem);
    let dir_delete = format!("{problems_dir}/{problem_dir}");
    fs::remove_dir_all(dir_delete).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Could not delete Problem Directory: {e}")
                .red()
                .bold()
        );
        process::exit(1);
    });
    println!("{}", "Deleted Problem Directory!\n".green().bold());

    println!("{}", "Deleting Problem from DB ...");
    delete_problem_db(db_conn, id);
    println!("{}", "Deleted Problem from DB!\n".green().bold());

    post_problem_delete_hook(&problem);
}
