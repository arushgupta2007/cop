use std::process::{Command, Output};

use chrono::{
    DateTime, Datelike, Local, NaiveDate, NaiveDateTime, NaiveTime, ParseError, TimeZone, Timelike,
};

use super::{contest::Contest, problem::Problem};

pub fn try_parse_datetime_from_string(src: &str) -> Result<DateTime<Local>, ParseError> {
    let ndt = NaiveDateTime::parse_from_str(src, "%H:%M %d/%m/%Y")?;
    let dt: DateTime<Local> = Local.from_local_datetime(&ndt).unwrap();
    Ok(dt)
}

pub fn get_contest_dir_from_contest(contest: &Contest) -> String {
    let name = contest.name.clone();
    let platform = contest.platform.clone();
    let date = contest.date.clone();

    let name = name.replace(" ", "-").replace("/", "-");
    let date_string = format!(
        "{:02}:{:02}--{:02}-{:02}-{:04}",
        date.hour(),
        date.minute(),
        date.day(),
        date.month(),
        date.year()
    );
    format!("{name}-{platform}-{date_string}")
}

pub fn get_problem_dir_from_problem(problem: &Problem) -> String {
    let name = problem.name.clone();
    let platform = problem.platform.clone();

    let name = name.replace(" ", "-").replace("/", "-");
    format!("{name}-{platform}")
}

pub fn conv_day_month_year_hour_min_to_datetime(
    day: u32,
    month: u32,
    year: i32,
    hour: u32,
    min: u32,
) -> DateTime<Local> {
    let ndt = NaiveDateTime::new(
        NaiveDate::from_ymd(year, month, day),
        NaiveTime::from_hms(hour, min, 0),
    );
    Local.from_local_datetime(&ndt).unwrap()
}

pub fn get_compile_command(problem_dir_path_str: &str) -> Output {
    Command::new("clang++")
        .arg("-Wall")
        .arg("-Wextra")
        .arg("-Wshadow")
        .arg("-D_GLIBCXX_ASSERTIONS")
        .arg("-DDEBUG")
        .arg("-ggdb3")
        .arg("-o")
        .arg(format!("{problem_dir_path_str}/main"))
        .arg(format!("{problem_dir_path_str}/main.cpp"))
        .output()
        .expect("Could not Compile File")
}

pub fn get_run_command(
    problem_dir_path_str: &str,
    problem_in_path: &str,
    program_out_path: &str,
) -> Output {
    Command::new("sh")
        .arg("-c")
        .arg(format!(
            "{problem_dir_path_str}/main < {problem_in_path} &> {program_out_path}"
        ))
        .output()
        .expect("Failed To Run Executable File")
}
