use std::{fs, path::Path, process};

use chrono::{Datelike, Timelike};
use colored::Colorize;
use directories::ProjectDirs;
use rusqlite::Connection;

use super::{contest::Contest, helper::conv_day_month_year_hour_min_to_datetime, problem::Problem};

pub fn db_config() -> Connection {
    let project_data_dir: String;
    if let Some(project_dirs) = ProjectDirs::from("com", "cop", "cop") {
        project_data_dir = project_dirs.data_dir().display().to_string();
    } else {
        println!("{}", "Could Not Get Data Directory of OS".red().bold());
        process::exit(1);
    };

    if !Path::new(&project_data_dir).exists() {
        fs::create_dir(&project_data_dir).unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Could Not Create Project Data Directory: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
    }

    let conn = Connection::open(format!("{project_data_dir}/cop.db")).unwrap_or_else(|e| {
        println!("{}", format!("Could Not Connect To DB: {e}").red().bold());
        process::exit(1);
    });
    conn.execute(
        "CREATE TABLE IF NOT EXISTS contests (
            id integer primary key,
            name text not null,
            platform text not null,
            problems integer not null,
            day integer not null,
            month integer not null,
            year integer not null,
            hour integer not null,
            min integer not null,
            solved integer not null
        )",
        [],
    )
    .unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Unable to create Contest Table in DB: {e}")
                .red()
                .bold()
        );
        process::exit(1);
    });
    conn.execute(
        "CREATE TABLE IF NOT EXISTS problems (
            id integer primary key,
            name text not null,
            platform text not null,
            solved integer not null,
            has_editorial integer not null
        )",
        [],
    )
    .unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Unable to create Problem Table in DB: {e}")
                .red()
                .bold()
        );
        process::exit(1);
    });

    conn
}

pub fn insert_contest_db(db_conn: &Connection, contest: &Contest) -> u32 {
    let name = contest.name.clone();
    let platform = contest.platform.clone();
    let no_problem = contest.no_problem.clone();
    let date = contest.date.clone();
    let solved = contest.solved.clone();
    let name = name.replace(" ", "-").replace("/", "-");
    db_conn.execute("INSERT INTO contests (name, platform, problems, day, month, year, hour, min, solved) values (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)", [name, platform, no_problem.to_string(), date.day().to_string(), date.month().to_string(), date.year().to_string(), date.hour().to_string(), date.minute().to_string(), solved.to_string()]).unwrap_or_else(|e| {
        println!("{}", format!("Unable to save Contest to DB: {e}").red().bold());
        process::exit(1);
    });
    db_conn
        .last_insert_rowid()
        .to_string()
        .parse()
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Could not get ID of created contest: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        })
}

pub fn get_contest_by_id(db_conn: &Connection, id: u32) -> Option<Contest> {
    let mut stmt = db_conn
        .prepare("SELECT id, name, platform, problems, day, month, year, hour, min, solved FROM contests WHERE id = ?")
        .unwrap_or_else(|e| {
            println!("{}", format!("Error Getting Contest By ID: {e}").red().bold());
            process::exit(1);
        });

    let mut rows = stmt.query([id]).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Error Getting Contest By ID: {e}").red().bold()
        );
        process::exit(1);
    });
    while let Some(row) = rows.next().unwrap() {
        let day = row.get(4).unwrap();
        let month = row.get(5).unwrap();
        let year = row.get(6).unwrap();
        let hour = row.get(7).unwrap();
        let min = row.get(8).unwrap();
        let date = conv_day_month_year_hour_min_to_datetime(day, month, year, hour, min);
        return Some(Contest {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            platform: row.get(2).unwrap(),
            no_problem: row.get(3).unwrap(),
            date,
            solved: row.get(9).unwrap(),
        });
    }

    None
}

pub fn get_list_contest_db(db_conn: &Connection) -> Vec<Contest> {
    let mut query = db_conn
        .prepare("SELECT id, name, platform, problems, day, month, year, hour, min, solved FROM contests;")
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Listing all Contests: {e}").red().bold()
            );
            process::exit(1);
        });
    let contests = query
        .query_map([], |row| {
            let day = row.get(4)?;
            let month = row.get(5)?;
            let year = row.get(6)?;
            let hour = row.get(7)?;
            let min = row.get(8)?;
            let dt = conv_day_month_year_hour_min_to_datetime(day, month, year, hour, min);
            Ok(Contest {
                id: row.get(0)?,
                name: row.get(1)?,
                platform: row.get(2)?,
                no_problem: row.get(3)?,
                date: dt,
                solved: row.get(9)?,
            })
        })
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Listing all Contests: {e}").red().bold()
            );
            process::exit(1);
        });
    let mut res: Vec<Contest> = vec![];

    for contest in contests {
        let curr_contest = contest.unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Listing all Contests: {e}").red().bold()
            );
            process::exit(1);
        });
        res.push(curr_contest);
    }

    res
}

pub fn set_solved_contest_db(db_conn: &Connection, id: u32, no_solved: u8) {
    db_conn
        .execute(
            "UPDATE contests SET solved = ?1 WHERE id = ?2",
            [no_solved.to_string(), id.to_string()],
        )
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while saving to DB: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
}

pub fn delete_contest_db(db_conn: &Connection, id: u32) {
    db_conn
        .execute("DELETE FROM contests WHERE id = ?1", [id])
        .unwrap_or_else(|e| {
            println!("{}", format!("Could not delete from DB: {e}").red().bold());
            process::exit(1);
        });
}

pub fn insert_problem_db(db_conn: &Connection, problem: &Problem) -> u32 {
    let name = problem.name.clone();
    let platform = problem.platform.clone();
    let solved = problem.solved.clone();
    let has_editorial = problem.has_editorial.clone();
    let name = name.replace(" ", "-").replace("/", "-");
    db_conn
        .execute(
            "INSERT INTO problems (name, platform, solved, has_editorial) values (?1, ?2, ?3, ?4)",
            [
                name,
                platform,
                solved.to_string(),
                has_editorial.to_string(),
            ],
        )
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Unable to save Contest to DB: {e}").red().bold()
            );
            process::exit(1);
        });
    db_conn
        .last_insert_rowid()
        .to_string()
        .parse()
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Could not get ID of created problem: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        })
}

pub fn get_list_problem_db(db_conn: &Connection) -> Vec<Problem> {
    let mut query = db_conn
        .prepare("SELECT id, name, platform, solved, has_editorial FROM problems;")
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Listing all Problems: {e}").red().bold()
            );
            process::exit(1);
        });
    let problems = query
        .query_map([], |row| {
            Ok(Problem {
                id: row.get(0)?,
                name: row.get(1)?,
                platform: row.get(2)?,
                solved: row.get(3)?,
                has_editorial: row.get(4)?,
            })
        })
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Listing all Contests: {e}").red().bold()
            );
            process::exit(1);
        });
    let mut res: Vec<Problem> = vec![];

    for problem in problems {
        let curr_problem = problem.unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Listing all Contests: {e}").red().bold()
            );
            process::exit(1);
        });
        res.push(curr_problem);
    }

    res
}

pub fn get_problem_by_id(db_conn: &Connection, id: u32) -> Option<Problem> {
    let mut stmt = db_conn
        .prepare("SELECT id, name, platform, solved, has_editorial FROM problems WHERE id = ?")
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error Getting Problem By ID: {e}").red().bold()
            );
            process::exit(1);
        });

    let mut rows = stmt.query([id]).unwrap_or_else(|e| {
        println!(
            "{}",
            format!("Error Getting Problem By ID: {e}").red().bold()
        );
        process::exit(1);
    });
    while let Some(row) = rows.next().unwrap() {
        return Some(Problem {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            platform: row.get(2).unwrap(),
            solved: row.get(3).unwrap(),
            has_editorial: row.get(4).unwrap(),
        });
    }

    None
}

pub fn toggle_solved_problem_by_id(db_conn: &Connection, id: u32, initial_val: u8) {
    db_conn
        .execute(
            "UPDATE problems SET solved = ?1 WHERE id = ?2",
            [(1 - initial_val).to_string(), id.to_string()],
        )
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while toggling solved state in DB: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
}

pub fn toggle_has_editorial_problem_by_id(db_conn: &Connection, id: u32, initial_val: u8) {
    db_conn
        .execute(
            "UPDATE problems SET has_editorial = ?1 WHERE id = ?2",
            [(1 - initial_val).to_string(), id.to_string()],
        )
        .unwrap_or_else(|e| {
            println!(
                "{}",
                format!("Error occurred while toggling editorial state in DB: {e}")
                    .red()
                    .bold()
            );
            process::exit(1);
        });
}

pub fn delete_problem_db(db_conn: &Connection, id: u32) {
    db_conn
        .execute("DELETE FROM problems WHERE id = ?1", [id])
        .unwrap_or_else(|e| {
            println!("{}", format!("Could not delete from DB: {e}").red().bold());
            process::exit(1);
        });
}
