{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, flake-utils, naersk, ... }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages."${system}";
        naersk-lib = naersk.lib."${system}";
      in
        rec {
          # `nix build`
          packages.cop = naersk-lib.buildPackage {
            pname = "cop";
            root = ./.;
            buildInputs = with pkgs; [ sqlite ];
          };
          defaultPackage = packages.cop;

          # `nix run`
          apps.cop = flake-utils.lib.mkApp {
            drv = packages.cop;
          };
          defaultApp = apps.cop;

          # `nix develop`
          devShell = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [ 
              rustc 
              cargo 
              rust-analyzer 
              rustfmt 
            ];
          };
        }
    );
}
